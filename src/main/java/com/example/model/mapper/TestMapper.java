package com.example.model.mapper;

import com.example.persistence.TestEntity;
import com.example.model.dto.TestDTO;

public class TestMapper {
    public static TestEntity toEntity(TestDTO test){
        return TestEntity.builder()
                .testString(test.getTestString())
                .id(test.getId())
                .build();
    }

    public static TestDTO toDTO(TestEntity test){
        return TestDTO.builder()
                .testString(test.getTestString())
                .id(test.getId())
                .build();
    }
}
