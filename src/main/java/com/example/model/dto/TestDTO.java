package com.example.model.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TestDTO {
    private Long id;
    private String testString;
}
