package com.example.config;

import com.example.model.dto.TestDTO;
import com.example.service.TestService;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Singleton
@Startup
public class InitTestData {

    @Inject
    TestService testService;

    @PostConstruct
    public void load() {
        for (long i = 1; i < 6; i++) {
            testService.createTestObject(TestDTO.builder().id(i).testString("test objekat " + i).build());
        }

    }
}
