package com.example.service;


import com.example.persistence.TestEntity;
import com.example.model.mapper.TestMapper;
import com.example.model.dto.TestDTO;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RequestScoped
@Transactional
public class TestService {

    @PersistenceContext
    EntityManager entityManager;

    public Long createTestObject(TestDTO test){
        TestEntity newEntity = TestMapper.toEntity(test);
        entityManager.persist(newEntity);
        return newEntity.getId();
    }

    public List<TestDTO> getTestObjects(){
        List<TestEntity> allTestEntities = entityManager.createQuery("SELECT t from TestEntity t", TestEntity.class)
                .getResultList();
        return allTestEntities.stream().map(TestMapper::toDTO).collect(Collectors.toList());
    }
}
