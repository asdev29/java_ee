package com.example.rest;


import com.example.model.dto.TestDTO;
import com.example.service.TestService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("test")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TestRest {

    @Inject
    TestService testService;

    @GET
    public Response getAllTestObjects() {
        return Response.ok(testService.getTestObjects()).build();
    }

    @POST
    public Response createTestObject(TestDTO test) {
        Long idOfCreatedObject = testService.createTestObject(test);
        return Response.ok(idOfCreatedObject).build();
    }

}
