#  Jakarta EE JAXRS Sample


## Instructions

1. Build and Run on Payara server

   ```bash
    mvn clean package cargo:run
   ```
2. Wait until server is started and following message is shown
   ```bash
    [INFO] [talledLocalContainer] Payara 5.2022.1 started on port [8080]
    [INFO] Press Ctrl-C to stop the container...
   ```
3. Go to http://localhost:8080/udemy-ee/api/v1/test and you will get 5 test objects
5. For DB access go to http://localhost:8080/udemy-ee/h2
   1. JDBC-URL: jdbc:h2:mem:test
   2. username: sa
   3. password: <no password>
   4. Click on Connect
6. For server logs navigate to folder \target\payara5\glassfish\domains\domain1\logs\ and open server.log file

  
